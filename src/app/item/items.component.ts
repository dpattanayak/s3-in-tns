import { Component, OnInit, NgZone } from "@angular/core";
import { S3 } from "nativescript-aws-sdk/s3";
import { Progress } from "tns-core-modules/ui/progress";
import {
    ImageSource,
    fromFile,
    fromResource,
    fromBase64
} from "tns-core-modules/image-source";
import { knownFolders, Folder, File, path } from "tns-core-modules/file-system";
import { CameraService } from "./camera.service";

S3.init({
    endPoint: "s3.amazonaws.com",
    accessKey: "AKIAIVNWWQTBQ44HF26A",
    secretKey: "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",
    type: "static"
}); // <= Try calling this before the app starts

@Component({
    selector: "ns-items",
    templateUrl: "./items.component.html"
})
export class ItemsComponent implements OnInit {
    uploadingFileID: any;
    remoteUrl: string;
    myProgressBar: any;
    isComplete: Boolean = false;
    selectedFile: any;
    selectedFileName: string;
    errorMessage: string;
    isError:Boolean = false;

    constructor(private cameraService: CameraService, private zone: NgZone) {}

    ngOnInit(): void {
        // this.selectedFolder = knownFolders.currentApp().getFolder("assets");
        // this.selectedFile = `${this.selectedFolder.path}/fly-image.png`;
        // console.log(JSON.stringify(this.selectedFile), "<< imgLocation");
    }

    onProgressBarLoaded(args: any) {
        let myProgressBar = args.object as Progress;

        myProgressBar.value = 0; // Initial value
        myProgressBar.maxValue = 100; // Maximum value

        this.myProgressBar = myProgressBar;
    }

    tapCameraAction = () => {
        this.selectedFile = '';
        this.selectedFileName = '';
        this.cameraService
            .takePicture()
            .then(result => {
                result = result as JSON;
                console.log("tapCameraAction >>>", result["cropped"]);
                this.zone.run(() => {
                    let base64 = "data:image/jpg;base64," + result["cropped"];
                    this.saveTofile(base64);
                });
            })
            .catch(result => {
                console.log(result);
            });
    };

    imagePicker(useGallery: boolean) {
        this.selectedFile = '';
        this.selectedFileName = '';
        this.cameraService
            .takePicture(useGallery)
            .then(result => {
                result = result as JSON;
                this.zone.run(() => {
                    this.saveTofile(result["cropped"]);
                });
            })
            .catch(result => {
                console.log(result);
            });
    }

    saveTofile(base64: any) {
        ImageSource.fromBase64(base64)
            .then((imageSource: ImageSource) => {
                const folderPath: string = knownFolders.currentApp().getFolder("assets").path;
                const fileName = "fly-image.png";
                const filePath = path.join(folderPath, fileName);
                console.log('filePath >>>>',filePath)
                const saved: boolean = imageSource.saveToFile(filePath, "png");
                if (saved) {
                    console.log("Image saved successfully!");
                    this.selectedFile = filePath;
                    this.selectedFileName = fileName;
                    this.uploadFile();
                }
            })
            .catch((e) => {
                console.log("Error: ");
                console.log(e);
            });
    }

    uploadFile() {
        const s = new S3();
        this.uploadingFileID = s.createUpload({
            file: "~/assets/fly-image.png",
            bucketName: "takecareaistorage",
            key:"flyUpload" +"/" +"fly-Profile" +Math.round(Math.random() * 10000000),
            acl: "public-read",
            completed: (error: any, success: any) => {
                if (error) {
                    console.log(`Upload Failed :-> ${error.message}`);
                    this.errorMessage = error.message;
                    this.isError = true;
                }
                if (typeof success != "undefined" && success) {
                    if (typeof success.path != "undefined" && success.path) {
                        this.zone.run(() => {
                            this.remoteUrl = success.path;
                            console.log("Upload Complete", this.remoteUrl);
                            this.isComplete = true;
                        });
                    }
                }
            },
            progress: (progress: any) => {
                console.log("uploadFile Progress", progress.value);
                this.myProgressBar.value = progress.value;
            }
        });
        console.log("this.uploadingFileID", this.uploadingFileID);
    }
}