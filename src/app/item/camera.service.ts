import { Injectable } from "@angular/core";

import { ImageCropper } from "nativescript-imagecropper";
import * as camera from "nativescript-camera";
import * as permissions from "nativescript-permissions";

import * as CImageSource from "tns-core-modules/image-source";
declare var android: any;
import * as imagepicker from "nativescript-imagepicker";
import { ImageSource } from "tns-core-modules/image-source";
// import { isAndroid, isIOS } from "platform";

//import { Observable } from "tns-core-modules/data/observable";
import { device, screen, isAndroid, isIOS } from "tns-core-modules/platform";

@Injectable()
export class CameraService {
    cameraAvailable: Boolean;
    imageCropper: ImageCropper;

    constructor() {
        this.cameraAvailable = camera.isAvailable();
        this.imageCropper = new ImageCropper();
    }

    takePicture(useGallery = false) {
        console.log("inside camera service takePicture");
        return new Promise((resolve, reject) => {
            if (isAndroid) {
                console.log("inside camera service is android");
                permissions
                    .requestPermission([
                        android.Manifest.permission.CAMERA,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ])
                    .then(() => {
                        console.log("inside camera service to crop");
                        resolve(
                            this.cropImage({ lockSquare: true }, useGallery)
                        );
                    })
                    .catch(() => {
                        // When user denies permission
                        console.log("User denied permissions");
                        reject(-1);
                    });
            } else {
                if (!isAndroid) {
                    // to make it work in iOS emulator
                    setTimeout(() => {
                        // on iOS we want a timeout of 1second as it takes time before
                        // the imageSource is ready to be read by the plugin
                        resolve(this.cropImage({ lockSquare: true }));
                    }, 1000);
                }
            }
        });
    }

    cropImage(options, useGallery = false) {
        console.log("inside camera service cropImage()", useGallery);

        return new Promise((resolve, reject) => {
            //var useGallery = true;

            if (useGallery) {
                console.log("use gallery");
                let context = imagepicker.create({
                    mode: "single" // use "multiple" for multiple selection
                });
                context
                    .authorize()
                    .then(() => {
                        return context.present();
                    })

                    .then(selection => {
                        selection.forEach(imageAsset => {
                            console.log(
                                "inside camera service took Picture now"
                            );

                            ImageSource.fromAsset(imageAsset).then(
                                imgsource => {
                                    console.log(
                                        "camimgsourceaaaa",
                                        imgsource.width
                                    );

                                    const originalimage = imgsource.toBase64String(
                                        "jpeg",
                                        100
                                    );
                                    console.log(
                                        "camimgsource2233",
                                        imgsource.width,
                                        imgsource.height
                                    );

                                    setTimeout(
                                        () => {
                                            this.imageCropper
                                                .show(imgsource, options)
                                                .then(args => {
                                                    const image = args.image.toBase64String(
                                                        "jpeg",
                                                        100
                                                    );
                                                    var res = {
                                                        cropped: image,
                                                        original: originalimage,
                                                        width: imgsource.width.toString(),
                                                        height: imgsource.height.toString()
                                                    };
                                                    console.log("request done");
                                                    resolve(res);
                                                })
                                                .catch(e => {
                                                    console.log(
                                                        "inside camera service error"
                                                    );
                                                    console.log(e);
                                                    reject(0);
                                                });
                                        },
                                        isAndroid ? 0 : 1000
                                    );
                                }
                            );
                        });
                    })
                    .catch(function(e) {
                        console.log(e);
                        // process error
                    });
            } else {
                // width: 300,
                //     height: 300,

                console.log("use camera");
                camera
                    .takePicture({
                        keepAspectRatio: true,
                        saveToGallery: false
                    })
                    .then(imageAsset => {
                        console.log("inside camera service took Picture now");

                        const source = new CImageSource.ImageSource();

                        source.fromAsset(imageAsset).then(imgsource => {
                            console.log(
                                "camimgsourcea width",
                                imgsource.width,
                                imgsource.height
                            );

                            const originalimage = imgsource.toBase64String(
                                "jpeg",
                                100
                            );
                            console.log("camimgsourceaaa");
                            //const originalbase64image = "data:image/jpeg;base64," + originalimage;

                            setTimeout(
                                () => {
                                    this.imageCropper
                                        .show(imgsource, options)
                                        .then(args => {
                                            const image = args.image.toBase64String(
                                                "jpeg",
                                                100
                                            );
                                            //const base64image = "data:image/jpeg;base64," + image;
                                            var res = {
                                                cropped: image,
                                                original: originalimage,
                                                width: imgsource.width.toString(),
                                                height: imgsource.height.toString()
                                            };
                                            console.log("request done");
                                            resolve(res);
                                        })
                                        .catch(e => {
                                            console.log(
                                                "inside camera service error"
                                            );
                                            console.log(e);
                                            reject(0);
                                        });
                                },
                                isAndroid ? 0 : 1000
                            );
                        });
                    })
                    .catch(err => {
                        console.log("Error -> " + err.message);
                        reject(0);
                    });
            }
        });
    }
}
